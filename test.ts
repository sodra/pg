import PG from './dist'

const pg = new PG()

type UserRow = {
  user_id: string
}

const test = async () => {
  const { rows } = await pg.select<UserRow>('users')

  for (let row of rows) {
    console.log(row.user_id)
  }
}
