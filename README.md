# @sodra/pg

## Install

```
npm i @sodra/pg
```

## Usage

```
import PG from '@sodra/pg'

const pg = new PG({
  connectionString: ...,
})

type User = {
  user_id: string
}

const { rows } = await pg.query<User>(`SELECT * FROM users WHERE user_id=$userId`, { userId })
```

## Simple queries

```
// SELECT * FROM users WHERE user_id=$userId
pg.select('users', { userId })

// SELECT name FROM users WHERE user_id=$userId
pg.select('users', 'name', { userId })

// SELECT count(1) FROM users WHERE user_id=$userId
pg.count('users', { userId })

// INSERT INTO users (user_id, name) VALUES ($userId, $userName)
pg.insert('users', { userId, name })

// UPDATE users SET name=$name WHERE user_id=$userId
pg.update('users', { name }, { userId })

// DELETE FROM users WHERE user_id=$userId
pg.del('users', { userId })
```

## Transactions

```
const batch = pg.beginBatch()

batch.insert('...'),
batch.update('...'),
batch.del('...')

await batch.exec()
```
