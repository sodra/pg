import pg from 'pg'

export type FieldValue = any

export type Fields = Record<string, FieldValue>

export type BatchQuery = {
  query: string
  values?: FieldValue[]
}

export type Batch = {
  exec: () => void
  query: (query: string, values?: Fields) => Batch
  insert: (table: string, fields: Fields) => Batch
  update: (table: string, fields: Fields, conditionFields?: Fields) => Batch
  del: (table: string, conditionFields?: Fields) => Batch
  size: () => number
}

export type Transaction = {
  query: (query: string, values?: FieldValue[]) => Promise<pg.QueryResult>
  commit: () => Promise<void>
  rollback: () => Promise<void>
  commitOrRollback: () => Promise<void>
}

const snakeCase = (str = '') =>
  str
    .split('')
    .map((char, index) => {
      const lowerCase = char.toLowerCase()
      return index === 0 || char === lowerCase ? lowerCase : `_${lowerCase}`
    })
    .join('')

export class PG {
  private pool

  constructor(config?: pg.PoolConfig) {
    this.pool = new pg.Pool(config)
  }

  async destroy() {
    return this.pool.end()
  }

  private runQuery = async ({ query, values }: { query: string; values?: FieldValue[] }) => {
    const client = await this.pool.connect()
    try {
      return await client.query(query, values)
    } catch (err) {
      console.error('QUERY FAILED: ', query, values)
      throw err
    } finally {
      client.release()
    }
  }

  private customQuery = (query: string, params: Fields = {}) => {
    const values = []
    for (let [key, value] of Object.entries(params)) {
      if (query.includes(`$${key}`)) {
        const regex = new RegExp(`\\$${key}\\b`, 'g')
        query = query.replace(regex, `$${values.length + 1}`)
        values.push(value)
      }
    }
    return { query, values }
  }

  private selectQuery(
    table: string,
    selectFields: string,
    conditionFields?: Fields
  ): { query: string; values?: FieldValue[] } {
    let query = `SELECT ${selectFields} FROM ${table}`
    let values

    if (conditionFields !== undefined) {
      const condition = Object.keys(conditionFields)
        .map((name, i) => `${snakeCase(name)}=$${i + 1}`)
        .join(' AND ')
      query += ` WHERE ${condition}`
      values = Object.values(conditionFields)
    }

    return { query, values }
  }

  private countQuery = (table: string, conditionFields?: Fields) => {
    let query = `SELECT count(1) FROM ${table}`
    let values
    if (conditionFields !== undefined) {
      const condition = Object.keys(conditionFields)
        .map((name, i) => `${snakeCase(name)}=$${i + 1}`)
        .join(' AND ')
      query += ` WHERE ${condition}`
      values = Object.values(conditionFields)
    }
    return {
      query,
      values
    }
  }

  private insertQuery = (table: string, fields: Fields) => {
    const columns = Object.keys(fields)
      .map((name) => snakeCase(name))
      .join(', ')
    const dollars = Object.keys(fields)
      .map((_, i) => `$${i + 1}`)
      .join(', ')
    const values = Object.values(fields)
    return {
      query: `INSERT INTO ${table} (${columns}) VALUES (${dollars})`,
      values
    }
  }

  private updateQuery = (table: string, fields: Fields, conditionFields?: Fields) => {
    const numColumns = Object.keys(fields).length
    const columns = Object.keys(fields)
      .map((name, i) => `${snakeCase(name)}=$${i + 1}`)
      .join(', ')

    let query = `UPDATE ${table} SET ${columns}`
    let values = Object.values(fields)

    if (conditionFields !== undefined) {
      const condition = Object.keys(conditionFields)
        .map((name, i) => `${snakeCase(name)}=$${numColumns + i + 1}`)
        .join(' AND ')
      query += ` WHERE ${condition}`
      values = [...values, ...Object.values(conditionFields)]
    }

    return { query, values }
  }

  private deleteQuery = (table: string, conditionFields?: Fields) => {
    let query = `DELETE FROM ${table}`
    let values

    if (conditionFields !== undefined) {
      const condition = Object.keys(conditionFields)
        .map((name, i) => `${snakeCase(name)}=$${i + 1}`)
        .join(' AND ')
      query += ` WHERE ${condition}`
      values = Object.values(conditionFields)
    }

    return { query, values }
  }

  query<T extends pg.QueryResultRow = any>(
    query: string,
    values?: Fields
  ): Promise<pg.QueryResult<T>> {
    return this.runQuery(this.customQuery(query, values))
  }

  select<T extends pg.QueryResultRow = any>(
    table: string,
    selectOrConditionFields?: string | Fields,
    conditionFields?: Fields
  ): Promise<pg.QueryResult<T>> {
    if (typeof selectOrConditionFields === 'string') {
      return this.runQuery(this.selectQuery(table, selectOrConditionFields, conditionFields))
    }
    return this.runQuery(this.selectQuery(table, '*', selectOrConditionFields))
  }

  async count(table: string, conditionFields?: Fields): Promise<number> {
    const { rows } = await this.runQuery(this.countQuery(table, conditionFields))
    return Number(rows[0].count)
  }

  insert<T extends pg.QueryResultRow = any>(
    table: string,
    fields: Fields
  ): Promise<pg.QueryResult<T>> {
    return this.runQuery(this.insertQuery(table, fields))
  }

  update<T extends pg.QueryResultRow = any>(
    table: string,
    fields: Fields,
    conditionFields?: Fields
  ): Promise<pg.QueryResult<T>> {
    return this.runQuery(this.updateQuery(table, fields, conditionFields))
  }

  del<T extends pg.QueryResultRow = any>(
    table: string,
    conditionFields?: Fields
  ): Promise<pg.QueryResult<T>> {
    return this.runQuery(this.deleteQuery(table, conditionFields))
  }

  async beginTransaction(): Promise<Transaction> {
    const client = await this.pool.connect()
    await client.query('BEGIN')
    const query = (query: string, values?: any[]) => client.query(query, values)
    const commit = async () => {
      try {
        await client.query('COMMIT')
      } finally {
        client.release()
      }
    }
    const rollback = async () => {
      try {
        await client.query('ROLLBACK')
      } finally {
        client.release()
      }
    }
    const commitOrRollback = async () => {
      try {
        await commit()
      } catch (err) {
        await rollback()
        throw err
      }
    }
    return {
      query,
      commit,
      rollback,
      commitOrRollback
    }
  }

  beginBatch(): Batch {
    const queries: BatchQuery[] = []
    const batch: Partial<Batch> = {}

    batch.exec = async () => {
      if (queries.length === 0) {
        return
      }
      const t = await this.beginTransaction()
      try {
        for (let { query, values } of queries) {
          await t.query(query, values)
        }
        await t.commit()
      } catch (err) {
        await t.rollback()
        throw err
      }
    }

    batch.query = (query, values) => {
      queries.push(this.customQuery(query, values))
      return batch as Batch
    }

    batch.insert = (table, fields) => {
      queries.push(this.insertQuery(table, fields))
      return batch as Batch
    }

    batch.update = (table, fields, conditionFields) => {
      queries.push(this.updateQuery(table, fields, conditionFields))
      return batch as Batch
    }

    batch.del = (table, conditionFields) => {
      queries.push(this.deleteQuery(table, conditionFields))
      return batch as Batch
    }

    batch.size = () => queries.length

    return batch as Batch
  }
}

export default PG
